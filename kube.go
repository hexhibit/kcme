//go:generate goversioninfo -icon=kcme.ico
package main

import (
	"fmt"
	"github.com/fatih/color"
	"io/ioutil"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

type Controller struct {
	KubeClient *kubernetes.Clientset
	namespaces []string
}

func NewKubeController() Controller {
	inCluster := os.Getenv("IN_CLUSTER")
	var clientSet *kubernetes.Clientset
	if inCluster == "true" {
		clientSet = fromInsideCluster()
	} else {
		clientSet = fromOutsideCluster()
	}

	controller := Controller{
		KubeClient: clientSet,
	}
	nsList, err := clientSet.CoreV1().Namespaces().List(metav1.ListOptions{})
	if err != nil {
		panic("could not get namespaces from cluster: " + err.Error())
	}

	namespaces := make([]string, len(nsList.Items))
	for i, n := range nsList.Items {
		namespaces[i] = n.Name
	}

	controller.namespaces = namespaces

	return controller

}

func fromInsideCluster() *kubernetes.Clientset {
	logger.Debugf("Load cluster config")
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
		panic(fmt.Sprintf("could not read incluster config from, reason: %v", err))
	}
	// creates the client set
	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(fmt.Sprintf("could not read incluster config from, reason: %v", err))
	}

	return clientSet
}

func fromOutsideCluster() *kubernetes.Clientset {
	logger.Debugf("Load local config from KUBECONFIG environment variable")

	kubeFilePath := os.Getenv("KUBECONFIG")
	configPath := kubeFilePath

	if strings.Contains(kubeFilePath, ";") {
		configs := strings.Split(kubeFilePath, ";")
		for i, ns := range configs {
			fmt.Printf("(%d) %s\n", i, ns)
		}

		var input string
		fmt.Scanln(&input)
		idx, err := strconv.Atoi(input)
		if err != nil {
			panic(err)
		}
		configPath = configs[idx]
	}

	config, err := clientcmd.BuildConfigFromFlags("", configPath)
	if err != nil {
		panic(fmt.Sprintf("could not read config from %s, reason: %v", configPath, err))
	}

	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(fmt.Sprintf("could not create clientset from %s, reason: %v", configPath, err))
	}

	return clientSet
}

func (c *Controller) ListConfigMaps() (configMaps map[string][]v1.ConfigMap) {
	configMaps = make(map[string][]v1.ConfigMap)
	for _, ns := range c.namespaces {
		logger.Debugf("list config maps in namespace: %s", ns)
		cmList, err := c.KubeClient.CoreV1().ConfigMaps(ns).List(metav1.ListOptions{})
		if err != nil {
			logger.Errorf("could not list config maps in namespace %s: %v", ns, err)
		} else {
			configMaps[ns] = cmList.Items
		}
	}

	return
}

func PrintConfigMaps(configMaps map[string][]v1.ConfigMap) (bool, v1.ConfigMap) {
	color.Magenta("\nNamespaces\n")

	var namespaces []string
	for k := range configMaps {
		namespaces = append(namespaces, k)
	}

	sort.Strings(namespaces)

	for i, ns := range namespaces {
		fmt.Printf("(%d) %s\n", i, ns)
	}

	var input string
	fmt.Scanln(&input)
	idx, err := strconv.Atoi(input)
	if err != nil {
		panic(err)
	}

	namespace := namespaces[idx]
	color.Magenta("\nConfig maps in %s\n", namespace)

	cms := configMaps[namespace]
	for i, cm := range cms {
		fmt.Printf("(%d) %s\n", i, cm.Name)
	}

	fmt.Scanln(&input)
	idx, err = strconv.Atoi(input)
	if err != nil {
		panic(err)
	}

	cm := cms[idx]

	color.Magenta("\nData in %s > %s \n", namespace, cm.Name)

	nData := 0
	var data []string
	for k := range cm.Data {
		fmt.Printf("(%d) %s \n", nData, k)
		data = append(data, k)
		nData += 1
	}

	fmt.Scanln(&input)
	idx, err = strconv.Atoi(input)
	if err != nil {
		panic(err)
	}

	dataKey := data[idx]

	color.Magenta("Content of %s > %s > %s\n\n", namespace, cm.Name, dataKey)
	//fmt.Printf("%s", cm.Data[dataKey])
	originalData := cm.Data[dataKey]
	modifiedData, err := editText(dataKey, originalData)
	if modifiedData == originalData {
		color.Yellow("data didn't change...")
		return false, cm
	}

	cm.Data[dataKey] = modifiedData
	return true, cm
}

func editText(dataKey, content string) (string, error) {
	err := ioutil.WriteFile(dataKey, []byte(content), 777)
	if err != nil {
		logger.Errorf("could not write file: %v", err)
		return "", err
	}
	defer os.Remove(dataKey)

	_, err = filepath.Abs(dataKey)
	if err != nil {
		logger.Errorf("could not get absolute filepath: %v", err)
		return "", err
	}

	cmd := exec.Command("code", "-w", "file", dataKey)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		logger.Fatalf("cmd.Run() failed with %s\n", err)
		return "", err
	}

	data, err := ioutil.ReadFile(dataKey)
	if err != nil {
		logger.Errorf("could not read file after closing edit: %v", err)
		return "", err
	}

	return string(data), nil
}
