package main

import (
	"fmt"
	"github.com/common-nighthawk/go-figure"
	"github.com/fatih/color"
	"github.com/inancgumus/screen"
	"github.com/sirupsen/logrus"
)

var logger logrus.FieldLogger

func main() {

	logger = logrus.New()

	controller := NewKubeController()

	for {
		figure.NewFigure("kc me", "doom", true).Print()
		color.Blue("kube config map editor - v0.1.1")

		configMaps := controller.ListConfigMaps()
		changed, cm := PrintConfigMaps(configMaps)

		if changed {
			ns := cm.Namespace
			_, err := controller.KubeClient.CoreV1().ConfigMaps(ns).Update(&cm)
			if err != nil {
				panic(fmt.Sprintf("could not update config map: %v", err))
			}
			color.Green("config map updated successful")
		}

		var input string
		fmt.Scanln(&input)

		screen.Clear()
		screen.MoveTopLeft()

	}
}
